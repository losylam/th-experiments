#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Process photometry data from Corot"""

import csv
import pickle

import numpy as np
import matplotlib.pyplot as plt

def import_data():
    tab = []
    ts = []
    flux = []
    norm_flux = []

    with open("./data/corot.csv", "r") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            tab.append(row)
        tab.pop(0)
        for line in tab:
            ts.append(float(line[0].replace(",", ".")))
            flux.append(float(line[1].replace(",", ".")))
            norm_flux.append(float(line[3].replace(",", ".")))

    return {"ts": np.array(ts),
            "flux": np.array(flux),
            "norm_flux": np.array(norm_flux)}


def read_pkl(filename):
    with open(filename, "rb") as f:
        data = pickle.load(f)
        return data
    return None


if __name__=="__main__":
    # data = import_data()
    data = read_pkl("data/data.pkl")
    fig, ax = plt.subplots()
    ax.plot(data["ts"], data["norm_flux"])

    plt.show()





