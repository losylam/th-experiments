let capture;
let pix = [];

let radius_star = 200;
let radius_planet = 5;

let margin = 50;
let l_capteur = 400;

let slider_x;
let slider_y;
let slider_x1;
let slider_y1;

let pos_x = 0;
let pos_y = 0;

let pos_ui1 = 4 * margin;

let pos_mes1_x = margin + l_capteur/2;
let pos_mes1_y = margin + l_capteur/2;

let pos_planete_x = margin + l_capteur/2 + 20;
let pos_planete_y = margin + l_capteur/2 + 30;

var socket;
var dist_star = 1;

var pg;
window.onload = function(){
    console.log("onload");
    socket = io();
    socket.on('connect', function(){
        socket.emit('message', {data: 'hello io'});
    });
}

function setup() {
    createCanvas(1000, 500);
    pg = createGraphics(1000, 800);
    pg.noStroke();
    textSize(32);
    frameRate(15);
    console.log("hello");
    slider_x = createSlider(0, 1000, 200);
    slider_x.position(2*margin + l_capteur, margin + 40);
    slider_x.style('width', '250px');
    slider_y = createSlider(0, 1000, 200);
    slider_y.position(2*margin + l_capteur, margin + 60);
    slider_y.style('width', '250px');

    slider_x1 = createSlider(0, l_capteur, l_capteur/2);
    slider_x1.position(2*margin + l_capteur, pos_ui1 + 40);
    slider_x1.style('width', '250px');
    slider_y1 = createSlider(0, l_capteur, l_capteur/2);
    slider_y1.position(2*margin + l_capteur, pos_ui1 + 60);
    slider_y1.style('width', '250px');
}

function draw() {
    background(0);
    pos_x = slider_x.value();
    pos_y = slider_y.value();
    pos_mes1_x = margin + slider_x1.value();
    pos_mes1_y = margin + slider_y1.value();
    draw_img(pos_x, pos_y);
    image(pg, 0, 0);
    draw_capteur();
    draw_corono(margin + l_capteur/2, margin + l_capteur/2);
    stroke(255, 0, 0);
    draw_mes(pos_mes1_x, pos_mes1_y);
    noStroke();
    fill(255);
    text("Position image", 2 * margin + l_capteur - 10, margin + 20);
    text("Position mesure", 2 * margin + l_capteur - 10, pos_ui1+20);

    if (frameCount % 5 == 0){

        let dist_planete = dist(pos_mes1_x, pos_mes1_y, pos_planete_x, pos_planete_y);
        if (dist_planete < 10){
            socket.emit('osc', data=['/mesure', map(dist_planete, 0, 10, 1, 0)]);
        }else{
            socket.emit('osc', data=['/mesure', 0]);
        }

        console.log('emit');
        socket.emit('osc', data=['/noise', dist_star]);
        let val = pg.get(pos_mes1_x, pos_mes1_y);
        // socket.emit('osc', data=['/mesure', val[0]/255.0]);
        // let val = pg.pixels(pos_mes1_x, pos_mes1_y);
        // console.log(val);
    }
}

function draw_mes(x, y){
    push();
    translate(x, y);
    line(-15, 0, -10, 0);
    line(15, 0, 10, 0);

    line(0, -15, 0, -10);
    line(0, 15, 0, 10);
    pop();
}

function draw_img(x, y){
    // push();
    // translate(x, y);
    noStroke();
    draw_gradient(x, y, radius_star);

    // fill(200);
    // ellipse(x, y, radius_star, radius_star);
    // pop();
}

function draw_gradient(x, y, rad) {
    var d = dist(x, y, margin + l_capteur/2, margin + l_capteur/2);
    let col_max = 1;
    if (d < 100){
        col_max = map(d, 0, 100, 0, 1);
        dist_star = col_max;
    }else{
        dist_star = 1;
    }

    pg.background(0);
    pg.fill(50);
    pg.ellipse(x + 20, y + 30, radius_planet, radius_planet);
    for (let r = rad; r > 0; r -= 4) {
        let col = map(r, 0, rad, 255, 0)*col_max;
        // console.log(r, col);
        pg.fill(255, col);
        pg.ellipse(x, y, r, r);
    }
}

function draw_capteur(){
    noStroke();
    fill(255, 100);
    rect(0, 0, margin, 1080);
    rect(margin, 0, l_capteur, margin);
    rect(margin, margin + l_capteur, l_capteur, margin);
    rect(margin, 2*(margin + l_capteur), l_capteur, margin);
    rect(margin, 3*(margin + l_capteur), l_capteur, 1000);
    rect(margin + l_capteur, 0, width, height);
}


function draw_corono(x, y){
    push();
    stroke(255);
    translate(x, y);
    line(-l_capteur/2, 0, l_capteur/2, 0);
    line(0, -l_capteur/2, 0, l_capteur/2);
    pop();
}
