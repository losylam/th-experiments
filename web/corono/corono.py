from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
import eventlet

from oscpy.client import OSCClient
from oscpy.server import OSCThreadServer

app = Flask(__name__)
app.config['SECRET_KEY'] = "c'est un secret!"
socketio = SocketIO(app)

##################
# Configuration ##
##################

debug = True


osc_client = OSCClient("0.0.0.0", 9999)

##################
# Flask         ##
##################


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('message')
def handle_message(message):
    print('received message: ',)
    print(message)
    emit('message', 'hello from flask')

@socketio.on('osc')
def handle_message(message):
    print('osc: ',)
    osc_client.send_message(bytes(message[0], 'utf-8'), message[1:])
    print(message)


if __name__ == "__main__":
    socketio.run(app, debug=debug)
