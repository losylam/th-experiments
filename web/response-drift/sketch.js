let capture;
let pix = [];


function setup() {
    createCanvas(320*2, 240*2);
    capture = createCapture(VIDEO);
    capture.size(320, 240);
    //capture.hide();
    for (let y = 0; y < 240; y += 1) {
        for (let x = 0; x < 320; x += 1) {
            pix.push(new Pix());
        }
    }
}

function draw() {
    background(255);
    // image(capture, 0, 0, 320, 240);
    // filter(INVERT);

    fill(0);
    // stroke(255);
    capture.loadPixels();
    const stepSize = round(constrain(mouseX / 8, 3, 32));
    for (let y = 0; y < 240; y += stepSize) {
        for (let x = 0; x < 320; x += stepSize) {
            const i = y * 320 + x;
            const darkness = (capture.pixels[i * 4]) / 255;
            // pix[i].update(darkness);
            pix[i].set_target(darkness);
            rect(x*2, y*2, stepSize*2-1, (2*stepSize -1)* (1-pix[i].val));
        }
    }
}

class Pix {
    constructor(){
        this.x = 0;
        this.y = 0;
        this.val = 0;
        this.target = 0;
    }

    set_target(target){
        this.target = target;
        if (this.val > this.target){
            this.val -= 0.01;
        }else{
            this.val += 0.01;
        }
    }

    update(val){
        if (val > 0.1){
            this.val += val/10;
        }else{
            this.val -= 0.05;
            // this.val = val;
        }
        if (this.val < 0){
            this.val = 0;
        }else if (this.val > 1){
            this.val = 1;
        }
    }
}

