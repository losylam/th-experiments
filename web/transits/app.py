
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
import eventlet

from oscpy.client import OSCClient
from oscpy.server import OSCThreadServer

app = Flask(__name__)
app.config['SECRET_KEY'] = "c'est un secret!"
socketio = SocketIO(app)

##################
# Configuration ##
##################

debug = True


# osc_client = OSCClient("0.0.0.0", 9999)
osc_client = OSCClient("localhost", 9999)

##################
# Flask         ##
##################


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/transit')
def transit():
    return render_template('transit.html')

@app.route('/celestial')
def celestial():
    return render_template('celestial.html')

@app.route('/modelisation')
def modelisation():
    return render_template('modelisation.html')

@app.route('/aladin')
def aladin():
    return render_template('aladin.html')


@app.route('/aladin/controler')
def ascension():
    return render_template('aladin_controler.html')


@app.route('/spectre')
def spectre():
    return render_template('spectre.html')


@app.route('/controler')
def controler():
    return render_template('controler.html')


@socketio.on('message')
def handle_message(message):
    print('received message: ',)
    print(message)
    emit('message', 'hello from flask')


@socketio.on('controler')
def handle_controler(data):
    print('controler: ', data)
    emit('controler', data, broadcast=True)


@socketio.on('coords')
def handle_coords(data):
    print('coords: ', data)
    emit('coords', data, broadcast=True)


@socketio.on('osc')
def handle_message(message):
    print('osc: ',)
    osc_client.send_message(bytes(message[0], 'utf-8'), message[1:])
    print(message)


if __name__ == "__main__":
    socketio.run(app, debug=debug)
