
let star_rad = 50;
let orbit = 200;
let radius = 3;

let planets = [];

function setup(){
    let num = floor(random(5, 7));

    createCanvas(windowWidth, windowHeight);
    for (let i = 0; i<num; i++){
        planets.push({orbit: random(50, 400),
                      radius: random(1, 8),
                      period: random(500, 3000),
                     });
    }
}

function draw(){
    background("black");
    translate(width/2, height/3);

    fill("yellow");
    rect(-star_rad/2, 0, star_rad, 5000);

    fill("orange");
    noStroke();
    ellipse(0, 0, 50, 50);

    for (const planet of planets){
        let t = millis()/planet.period;
        let x = planet.orbit * cos(t);
        let y = planet.orbit * sin(t);

        if (y > 0){
            fill("black");
            rect(x-planet.radius/2, y, planet.radius, 5000);
        }
    }
    for (const planet of planets){
        push();

        let pos = millis()/planet.period;


        rotate(pos);
        // translate(orbit, 0);




        fill("gray");
        ellipse(planet.orbit, 0, planet.radius, planet.radius);
        pop();
    }
}



