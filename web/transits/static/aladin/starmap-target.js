import * as utils from "../modules/th-utils.js";
import * as commons from "../modules/th-commons.js";
import * as nav from "./starmap-nav.js";

const fov_ratio = 5;
/**
 * convert a coords as list to a float
 */
function list2coord(l){
    let val_s = "";
    l.forEach((val, i) => {
        val_s += val;
        if (i == 1) val_s += ".";
    });
    return parseFloat(val_s);
}

/**
 * convert a ascension coords as list to a float
 * TODO: use dim2res
 */
function list2coord_asc(l){
    let val_f = l[0]*15 + l[1]/0.4 + l[2]/4 + l[3]/24 + l[4]/240;
    return val_f;
}

/**
 * convert a declinaison coords as list to a float
 * TODO: use dim2res
 */
function list2coord_dec(l, ns){
    let val_f = 0;
    if (ns < 0){
        val_f = l[0]*10 - l[1] - l[2]/6 - l[3]/60 - l[4]/60/6;
    }else{
        val_f = l[0]*10 + l[1] + l[2]/6 + l[3]/60 + l[4]/60/6;
    }
    return val_f;
}

function get_empty_coord(){
    let coord = [[],[]];
    for (let i=0; i<10; i++){
        coord[0].push(0);
        coord[1].push(0);
    }
    coord.push(1);
    return coord;
}

const dim2res = commons.dim2res;

/**
 * convert a coordinate from string to array
 * @function coord2list
 * @param  {String}   c  A coordinate as a string
 * @return {Array}       An array containing values from 10e1 to 10e-9
 */
function coord2list(c){
    let coord = [];
    let uni = "0";
    let dec = "0";
    let s = c.toString();
    if (s.indexOf(".") >= 0){
        uni, dec = s.split(".");
    }else{
        uni = s;
    }

    if (uni.length > 1){
        coord.push(parseInt(uni.slice(0, uni.length)));
        coord.push(parseInt([uni.length - 1]));
    }else{
        coord.push(0);
        coord.push(parseInt(uni));
    }

    dec.forEach((i) => coord.push(parseInt(i)));
    while (coord.length<10){
        coord.push(0);
    }

    return coord;
}

function init_coord_ok(){
    let coords_ok = [];
    for (let i=0; i<10;i++){
        coords_ok.push({0: false, 1: false, 2: false});
    }
    return coords_ok;
}

function get_target(coords_star, nav_container_id, starmap_in=undefined){

    const starmap = starmap_in;
    starmap.dim2res = dim2res;

    const app = new Vue({
        el: "#app",
        data: {
            coords: get_empty_coord(),
            coords_ok: init_coord_ok(),
            coords_star: coords_star,
            dim: 0,
            abs_dim_x: 15,
            abs_dim_y: 10,
            target: {x: 7.5, y: 5},
            grid_changed: true,
            active: false,
            south: false,
        },
        watch: {
            dim: function(val){
                console.log("[target] update dim");
                // this.abs_dim = utils.dim2dim(val);
                this.abs_dim_x = dim2res[val][0];
                this.abs_dim_y = dim2res[val][1];

                if (starmap){
                    console.log("[target] coord_check starmap");
                    starmap.target = [this.target.x, this.target.y];
                    starmap.dim = this.dim;
                    // starmap.abs_dim = this.abs_dim_y;
                    // starmap.abs_dim_x = this.abs_dim_x;
                    // starmap.abs_dim_y = this.abs_dim_y;
                    starmap.grid_changed = true;
                    starmap.change_grid();
                    if (this.dim > 1){
                        starmap.anim.start_anim([this.target.x, this.target.y], this.abs_dim_y*fov_ratio*10);
                    }
                }
                this.update_coords();
            },
        },
        methods: {
            set_active: function(){
                this.active = true;
                document.querySelector("#canvas-overlay").classList.add("visible");
            },

            get_abs_coords: function(){
                this.pos = [list2coord_asc(this.coords[0]),
                            list2coord_dec(this.coords[1], this.coords[2])];
                return this.pos;
            },

            update_coords: function(){
                console.log("[target] update coords");
                this.get_abs_coords();
                let x = this.pos[0];
                let y = this.pos[1];


                this.target.x = utils.floor_dim(x, this.abs_dim_x) + this.abs_dim_x/2;
                if (!this.south){
                    this.target.y = utils.floor_dim(y, this.abs_dim_y) + this.abs_dim_y/2;
                }else{
                    this.target.y = utils.ceil_dim(y, this.abs_dim_y) - this.abs_dim_y/2;
                }


                if (this.dim == 0){
                    let d = Math.pow(10, 1 - app.dim);

                    if (starmap){
                        console.log("[target] update starmap");
                        starmap.target = [this.target.x, this.target.y];
                        starmap.init_grid();
                        starmap.anim.start_anim([this.target.x, this.target.y]);
                        starmap.grid_changed = true;
                    }else{
                    }
                }else{
                    starmap.target = [this.target.x, this.target.y];
                    starmap.grid_changed = true;
                }
            },

            coord_moins: function(c, i){
                this.coords[c][i] -= 1;
                if (c == 1 && i == 0){
                    if (this.coords[c][i] == -1 && !this.south){
                        this.coords[c][i] = 0;
                        this.south = true;
                        this.coords[2] = -1;
                    }
                }
                if (this.coords[c][i] < 0){
                    if (i == 0 && c == 0){
                        this.coords[c][i] = 23;
                    }else if (i == 0 && c == 1){
                        if (this.coords[c][i] < -8){
                            this.coords[c][i] = -8;
                        }
                    }else if (i >0){
                        this.coords[c][i] = 0;
                    }
                }
                this.update_coords();
                let coord_out = this.coords[c][i];
                if (c == 1 && i == 0 && this.south){
                    coord_out = -coord_out;
                }
                return coord_out;
            },

            coord_plus: function(c, i){
                this.coords[c][i] += 1;
                if (c == 0){       // ascension
                    if (i == 0){
                        this.coords[c][i] = this.coords[c][i]%24;
                    }else if (i==1 || i==3){
                        this.coords[c][i] = Math.min(this.coords[c][i], 5);
                    }else{
                        this.coords[c][i] = Math.min(this.coords[c][i], 9);
                    }
                }else if (c == 1){ // declinaison
                    if (i==0){
                        if (this.south && this.coords[c][i] == 1){
                            this.south = false;
                            this.coords[2] = 1;
                            this.coords[c][i] = 0;
                        }
                        if (this.coords[c][i] > 8){
                            this.coords[c][i] = 8;
                        }
                    }else if(i==2){
                        this.coords[c][i] = Math.min(this.coords[c][i], 5);
                    }else{
                        this.coords[c][i] = Math.min(this.coords[c][i], 9);
                    }
                }else{
                    this.coords[c][i] = this.coords[c][i]%10;
                }
                this.update_coords();

                let coord_out = this.coords[c][i];
                if (c == 1 && i == 0 && this.south){
                    coord_out = -coord_out;
                }
                return coord_out;
            },

            coord_valid: function(c, i){
                console.log(this.coords_star[c]);
                if (c == 0){
                    if (this.coords[c][i] == this.coords_star[c][i]){
                        this.coords_ok[i][c] = true;
                        return true;
                    }
                }else{
                    if (this.coords[c][i] == this.coords_star[c][i] && this.coords[2] == this.coords_star[2]){
                        this.coords_ok[i][c] = true;
                        return true;
                    }

                }
                return false;
            },

            coord_check: function(i){
                if (this.coords_ok[i][0] && this.coords_ok[i][1]){
                    this.coords_ok[i][2] = true;

                    this.dim += 1;

                    return true;
                }
                return false;
            }
        }
    });

    const nav_container = document.querySelector(nav_container_id);
    const navelem = new nav.Nav(app, nav_container);

    return app;
}

export { get_target };
