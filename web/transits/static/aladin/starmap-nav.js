
class Nav{
    constructor(app_in, container){
        this.container = container;
        this.app = app_in;
        this.create_nav();
        this.create_master();

        return this;
    }

    create_master(){
        let container_master = document.querySelector("#th-module-B");
        let btn_start = document.createElement("a");
        btn_start.setAttribute("id", "th-nav-start");
        btn_start.setAttribute("class", "th-btn");
        btn_start.textContent = "Commencer";
        container_master.appendChild(btn_start);

        btn_start.addEventListener("click", () => {
            this.app.set_active();
            document.querySelector("#coord-0-0").classList.remove("coord-inactive");
            document.querySelector("#coord-1-0").classList.remove("coord-inactive");
            document.querySelector(".coord-ns").classList.add("coord-active");
            container_master.removeChild(btn_start);

            this.create_coords_master();

            document.querySelector("#coord-master-0-0").classList.remove("coord-inactive");
            document.querySelector("#coord-master-1-0").classList.remove("coord-inactive");
            document.querySelector("#coord-master-0-0").classList.add("coord-active");
            document.querySelector("#coord-master-1-0").classList.add("coord-active");
            document.querySelector(".coord-ns-master").classList.add("coord-active");

            document.querySelector("#coord-master-0-0 .coord-num").textContent = this.app.coords_star[0][0];
            let dec = this.app.coords_star[1][0];
            if (this.app.coords_star[2]<0){
                dec = - dec;
                document.querySelector(".coord-ns-master").textContent = "S";
            }
            document.querySelector("#coord-master-1-0 .coord-num").textContent = dec;
        });
    }

    create_coord_asc(elem, master=false){
        let coord;
        for (let i=0; i<5; i++){
            if (!master){
                coord = this.create_coord(0, i);
            }else{
                coord = this.create_coord_master(0, i);
            }
            elem.appendChild(coord);
            if (i==0){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                deg.textContent = "h";
                elem.appendChild(deg);
            }else if(i==2){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                deg.textContent = "m";
                elem.appendChild(deg);
            }else if(i==4){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                deg.textContent = "\'";
                elem.appendChild(deg);
            }
        }
    }

    create_coord_dec(elem, master=false){
        let coord;
        for (let i=0; i<6; i++){
            if (i==0){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                if (master){
                    deg.classList.add("coord-ns-master");
                }else{
                    deg.classList.add("coord-ns");
                }
                deg.textContent = "N";
                elem.appendChild(deg);
            }
            if (!master){
                coord = this.create_coord(1, i);
            }else{
                coord = this.create_coord_master(1, i);
            }
            elem.appendChild(coord);
            if (i==1){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                deg.textContent = "°";
                elem.appendChild(deg);
            }else if(i==3){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                deg.textContent = "\'";
                elem.appendChild(deg);
            }else if(i==5){
                let deg = document.createElement("div");
                deg.classList.add("coord-deg");
                deg.textContent = "\"";
                elem.appendChild(deg);
            }
        }
    }

    create_coords_master(ok){
        console.log("create control master", ok);
        let container_master = document.querySelector("#th-module-B");

        let container_master_asc = document.createElement("div");
        container_master_asc.classList.add("coord-master");
        container_master.appendChild(container_master_asc);
        this.create_coord_asc(container_master_asc, true);

        let container_master_dec = document.createElement("div");
        container_master_dec.classList.add("coord-master");
        container_master.appendChild(container_master_dec);
        this.create_coord_dec(container_master_dec, true);
    }

    create_nav(){
        let controler = this.container;
        let nav = document.createElement("div");
        nav.setAttribute("id", "nav");

        // create nav asc
        let container_asc = document.querySelector("#th-module-A");
        let h = document.createElement("h3");
        h.textContent = "Ascension droite";
        container_asc.appendChild(h);

        let container_coord_asc = document.createElement("div");
        container_asc.appendChild(container_coord_asc);
        this.create_coord_asc(container_coord_asc);

        // create nav dec
        let container_dec = document.querySelector("#th-module-C");
        h = document.createElement("h3");
        h.textContent = "Déclinaison";
        container_dec.appendChild(h);

        let container_coord_dec = document.createElement("div");
        container_dec.appendChild(container_coord_dec);
        this.create_coord_dec(container_coord_dec);
    }

    create_coord_master(c, i){
        let coord = document.createElement("div");
        coord.setAttribute("id", "coord-master-"+c+"-"+i);
        coord.classList.add("coord");
        coord.classList.add("coord-"+i);
        coord.classList.add("coord-inactive");

        let num = document.createElement("div");
        num.classList.add("coord-num");
        num.textContent = "?";

        coord.appendChild(num);

        return coord;
    }

    create_coord(c, i){
        let coord = document.createElement("div");
        coord.setAttribute("id", "coord-"+c+"-"+i);
        coord.classList.add("coord");
        coord.classList.add("coord-"+i);
        coord.classList.add("coord-inactive");

        let plus = document.createElement("div");
        plus.classList.add("coord-plus");
        plus.classList.add("coord-ctrl");
        plus.textContent = "+";

        let num = document.createElement("div");
        num.classList.add("coord-num");
        num.textContent = "0";

        plus.addEventListener("click", () => {
            num.textContent = this.app.coord_plus(c, i);
            if (c == 1 && i == 0){
                let ns = document.querySelector(".coord-ns");
                if (this.app.south){
                    ns.textContent = "S";
                }else{
                    ns.textContent = "N";
                }
            }
        });

        let moins = document.createElement("div");
        moins.classList.add("coord-moins");
        moins.classList.add("coord-ctrl");
        moins.textContent = "-";

        moins.addEventListener("click", () => {
            num.textContent = this.app.coord_moins(c, i);
            if (c == 1 && i == 0){
                let ns = document.querySelector(".coord-ns");
                if (this.app.south){
                    ns.textContent = "S";
                }else{
                    ns.textContent = "N";
                }
            }

        });

        let valid = document.createElement("div");
        valid.classList.add("coord-valid");
        valid.classList.add("coord-ctrl");
        valid.textContent = "v";

        valid.addEventListener("click", () => {
            if (this.app.coord_valid(c, i)){
                coord.classList.remove("coord-inactive");
                coord.classList.add("coord-ok");
                let master_coord = document.querySelector("#coord-master-"+c+"-"+i);
                master_coord.classList.add("coord-ok");
                if (c == 1 && i == 0){
                    document.querySelector(".coord-ns-master").classList.add("coord-ok");
                    document.querySelector(".coord-ns").classList.add("coord-ok");
                }
                if (this.app.coord_check(i)){
                    this.activate_coord(i+1);
                }
            }else{
                coord.classList.remove("coord-ok");
            }
        });

        coord.appendChild(plus);
        coord.appendChild(num);
        coord.appendChild(moins);
        coord.appendChild(valid);

        return coord;
    }

    activate_coord(i){
        document.querySelectorAll(".coord-"+i).forEach((e) => {
            e.classList.add("coord-active");
            e.classList.remove("coord-inactive");
        });

        document.querySelector("#coord-master-0-" + i + " .coord-num").textContent = this.app.coords_star[0][i];
        document.querySelector("#coord-master-1-" + i + " .coord-num").textContent = this.app.coords_star[1][i];
    };
};

export { Nav }
