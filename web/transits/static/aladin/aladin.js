import * as utils from "../modules/th-utils.js";
import * as target from "./starmap-target.js";
import * as draw from "./starmap-draw.js";
import * as dial from "./starmap-dial.js";


/////////////////////////
// INIT                //
/////////////////////////

let aladin;
let socket;
let anim;

/////////////////////////
// CONFIG              //
/////////////////////////

const send_osc = false;

let transition = false;

let sensor = [0.03/2, 0.02/2];

const range_star = [
    [24, 6, 10, 6, 10],
    [8, 10, 6, 10, 6]
];

function create_random_star(){
    let coords = [[],[]];
    for (let i =0;i<2;i++){
        for (let j=0; j<5; j++){
            let val_max = range_star[i][j];
            let val = Math.random()*val_max;
            coords[i].push(Math.floor(val));
        }
    }
    coords.push(-1);
    coords[1][0] = -coords[1][0];
    return coords;
}

let coords_star = create_random_star();

/////////////////////////
// MAIN                //
/////////////////////////



// socket = io();
// socket.on('connect', function(){
//     socket.emit('message', {data: 'hello io'});
// });

aladin = A.aladin('#aladin-lite-div', {
    cooFrame: "ICRSd",
    showReticle: false,
    showZoomControl: false,
    showGotoControl: false,
    showFullscreenControl: false,
    showLayersControl: false,
    showFrame: false,
    survey:"P/DSS2/color",
    fov:50
});
const aladin_div = document.querySelector("#aladin-lite-div");

const starmap = new draw.StarMapDraw("canvas-overlay", aladin_div.clientWidth, aladin_div.clientHeight, aladin);
const app = target.get_target(coords_star, "#th-modules", starmap);
aladin.gotoRaDec(app.target.x, app.target.y);

window.requestAnimationFrame(()=>starmap.draw());


const aladin_canvas = document.querySelector("#aladin-lite-div .aladin-imageCanvas");
const aladin_ctx = aladin_canvas.getContext("2d");

function get_col(){
    let w = 10;
    let n_pix = w*w;
    let dat = aladin_ctx.getImageData(aladin_canvas.width/2-w/2, aladin_canvas.height/2-w/2, w, w);
    let tot = 0;
    for (let i=0; i < n_pix; i++ ){
        tot += dat.data[i*4];
        tot += dat.data[i*4+1];
        tot += dat.data[i*4+2];
    }
    tot = tot / (3*n_pix) / 255;
    // socket.emit('osc', data=['/target',  tot]);
    return dat;
}

const dial_asc = new dial.DialAsc(document.querySelector(".th-dial-asc"), 600, 250, app, starmap);
const dial_dec = new dial.DialDec(document.querySelector(".th-dial-dec"), 600, 250, app, starmap);


// socket.on('coords', data => {
//     console.log("socket coords", data.data);
//     app.coords = data.data;
//     app.update_coords();
//     update_coords();
//     app.grid_changed = true;
// });

// };

// function fov_plus(){
//     if (transition && app.dim == 5){
//         console.log("Transitiontiontion");
//         document.querySelector("#overlay").classList.add('visible');
//         setTimeout(()=>{
//             window.location = "http://localhost:8000/transit";
//         }, 4000);
//     }
// }


// function get_target_val(x, y){
//     let d = dist(x, y, coords_star_abs.x, coords_star_abs.y);
//     d = Math.log(d);
//     d = utils.range(1.5, -7.8, 0, 1, d);

//     socket.emit('osc', data=['/dist',  d]);
//     console.log("dist", d);
// }


/////////////////////////
// DEBUG/TEST          //
/////////////////////////


document.addEventListener('keydown', (event) => {
    if (event.key == "a"){
        document.querySelector("#coord-0-" + app.dim + " .coord-plus").click();
    }else if (event.key == "q"){
        document.querySelector("#coord-0-" + app.dim + " .coord-moins").click();
    }else if (event.key == "w"){
        document.querySelector("#coord-0-" + app.dim + " .coord-valid").click();
    }else if (event.key == "z"){
        document.querySelector("#coord-1-" + app.dim + " .coord-plus").click();
    }else if (event.key == "s"){
        document.querySelector("#coord-1-" + app.dim + " .coord-moins").click();
    }else if (event.key == "x"){
        document.querySelector("#coord-1-" + app.dim + " .coord-valid").click();
    }
});

// for (let i=14; i<17; i+=0.05){
//     i = parseFloat(i.toFixed(3));
//     console.log("[sex]", i, utils.dec2sex(i));
// }

// for (let i=0; i<2; i+=1){
//     for (let j=0; j<60; j+=1){
//         for (let k=0; k<60; k+=10){
//             let sex = [i, j, k];
//             console.log("[dec]", sex, utils.sex2dec(sex));
//         }
//     }
// }

console.log("[floor]", utils.floor_dim(32, 15));
