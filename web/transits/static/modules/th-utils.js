
/////////////////////////
// UTILS               //
/////////////////////////

const lerp = (x, y, a) => x * (1 - a) + y * a;
const clamp = (a, min = 0, max = 1) => Math.min(max, Math.max(min, a));
const invlerp = (x, y, a) => clamp((a - x) / (y - x));
const range = (x1, y1, x2, y2, a) => lerp(x2, y2, invlerp(x1, y1, a));

const dist = (x1, y1, x2, y2) => Math.hypot(x2-x1, y2-y1);

const easeIn  = p => t => Math.pow(t, p);
const easeOut = p => t => (1 - Math.abs(Math.pow(t-1, p)));
const easeInOut = p => t => t<.5 ? easeIn(p)(t*2)/2 : easeOut(p)(t*2 - 1)/2+0.5;


const max = p => Math.max(p[0], p[1]);

/**
 * Round value according to absolute dimension precision
 * @function round_dim
 * @param {float}   val      A value to round
 * @param {float}   abs_dim  The precision to round
 * @return {float}           The rounded value
 */
function round_dim(val, abs_dim){
    return Math.round(val / abs_dim) * abs_dim;
}

/**
 * Floor value according to absolute dimension precision
 * @function floor_dim
 * @param {float}   val      A value to floor
 * @param {float}   abs_dim  The precision to floor
 * @return {float}           The floored value
 */
function floor_dim(val, abs_dim){
    return Math.floor(val / abs_dim) * abs_dim;
}

/**
 * Ceil value according to absolute dimension precision
 * @function ceil_dim
 * @param {float}   val      A value to ceil
 * @param {float}   abs_dim  The precision to ceil
 * @return {float}           The floored value
 */
function ceil_dim(val, abs_dim){
    return Math.ceil(val / abs_dim) * abs_dim;
}

function dim2dim(dim){
    return Math.pow(10, 1-dim);
}

/**
 * Convert decimal angle to sexagesimal angle
 * @function dec2sex
 * @param {float}   val      An angle in decimal
 * @return {array}           An array containing [H, minut, second]
 */
export function dec2sex(val){
    let h = Math.floor(val/15.0);
    let rest = val - h*15;
    let min = Math.floor(rest*4.0);
    rest = rest - min/4;
    let sec = rest*240;
    return [h, min, sec];
}

/**
 * Convert sexagesimal angle to decimal angle
 * @function sex2dec
 * @param {array}   val      An array representing an angle in sexagesimal [H, m, s]
 * @return {float}           An angle in decimal
 */
export function sex2dec(val){
    let dec = val[0]*15 + val[1]/4 + val[2]/240;
    return dec;
}


export { lerp, clamp, invlerp, range, dist, easeIn, easeOut, easeInOut, max, round_dim, floor_dim, ceil_dim, dim2dim }

