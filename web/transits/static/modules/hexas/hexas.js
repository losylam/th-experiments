const xmlns = "http://www.w3.org/2000/svg";

class Hexas{
    constructor(selector){
        this.hexas = [];
        this.svg = document.getElementById(selector);

        this.start_animation = undefined;

        this.width = window.innerHeight;
        this.height = window.innerHeight;

        const win_height = window.innerHeight;
        const win_width = window.innerWidth;
        if (win_height > win_width){
            this.width = win_width;
            this.height = win_width;
        }

        this.svg.setAttribute("width", this.width);
        this.svg.setAttribute("height", this.height);
        const back1 = document.createElementNS(xmlns, "image");

        back1.setAttribute("href", "/static/images/infrared_nb.jpg");
        back1.setAttribute("x", 0);
        back1.setAttribute("y", 0);
        back1.setAttribute("width", this.width);
        back1.setAttribute("height", this.height);
        this.svg.append(back1);


        const back = document.createElementNS(xmlns, "image");

        back.setAttribute("href", "/static/images/infrared.jpg");
        back.setAttribute("x", 0);
        back.setAttribute("y", 0);
        back.setAttribute("width", this.width);
        back.setAttribute("height", this.height);
        back.setAttribute("mask", "url(#th-mask)");
        // back.setAttribute("style", "fill: yellow");
        this.svg.append(back);

        this.g = document.createElementNS(xmlns, "g");

        // this.g.setAttribute("transform", "translate("+this.width/2+", "+this.height/2+")");

        this.mask = document.createElementNS(xmlns, "mask");
        this.mask.setAttribute("id", "th-mask");
        this.g.append(this.mask);
        this.svg.append(this.g);

        this.create_hexas();
    }

    create_hexas(){
        const margin = 2;

        const hexa_rad = this.height/11;

        const step_y = (hexa_rad) * Math.sqrt(3) ;
        const step_x = (hexa_rad) * (1.5);

        const num_x = [3, 4, 5, 4, 3];
        const star_y = [1, 1.5, 2, 1.5, 1];

        for (let i_x = -2; i_x < 3; i_x++){
            const num_y_moins = num_x[i_x+2];

            for (let i_y = 0; i_y < num_y_moins; i_y++) {
                let dec = -star_y[i_x+2] * step_y;

                let pos_x = i_x * step_x;
                let pos_y = i_y * step_y + dec;
                this.create_hexa(this.width/2 + pos_x, this.height/2 + pos_y, hexa_rad-margin);
            }
        }
    }

    create_hexa(x, y, r){
        const poly_g = document.createElementNS(xmlns, "g");

        let xx = this.width/2 - x;
        let yy = this.height/2 - y;
        let dist = Math.sqrt(xx*xx + yy*yy);

        let start = 0;
        if (dist > 0){
            start = dist * 10 + Math.random()*1000;
        }
        let end = (500 - dist) * 3 + Math.random()*1000;

        const new_hexa = {g: poly_g,
                          rot: 90,
                          x: x,
                          start: start,
                          end: end,
                          vel: 0.5 + Math.random()/2};

        poly_g.setAttribute("style", "transform: translate("+x+"px,0)rotateY(90deg)");
        poly_g.classList.add("th-hexagon");

        const poly = document.createElementNS(xmlns, "polygon");
        let points = "";
        for (let i=0; i<6; i++){
            const pos_x = r*Math.cos(i * Math.PI/3);
            const pos_y = y + r*Math.sin(i * Math.PI/3);
            points += pos_x + "," + pos_y + " ";

        }
        poly.setAttribute("points", points);
        poly_g.append(poly);
        this.hexas.push(new_hexa);
        this.mask.append(poly_g);
    }

    animate = ()=>{
        if (!this.start_animation) this.start_animation = new Date();
        console.log("animate");
        let cpt = 0;
        for (const hexa of this.hexas){
            if (new Date() - this.start_animation > hexa.start){
                hexa.rot = hexa.rot - hexa.vel;
                if (hexa.rot < 0){
                    hexa.rot = 0;
                    cpt += 1;
                }

                hexa.g.setAttribute("style", "transform: translate("+hexa.x+"px,0)rotateY("+hexa.rot+"deg);opacity:" + (90 - hexa.rot)/90+ ";");
                // hexa.g.setAttribute("style", "transform: translate("+hexa.x+"px,0)rotateY("+hexa.rot+"deg)scale("+scale+","+scale+");");
            }
        }
        if (cpt < 19){
            setTimeout(this.animate, 20);
        }else{
            this.start_animation = undefined;
            setTimeout(this.hide, 10);
        }
    }

    hide = () => {
        if (!this.start_animation) this.start_animation = new Date();

        let cpt = 0;
        for (const hexa of this.hexas){
            if (new Date() - this.start_animation > hexa.end){
                hexa.rot = hexa.rot + hexa.vel;
                if (hexa.rot > 90){
                    hexa.rot = 90;
                    cpt += 1;
                }

                hexa.g.setAttribute("style", "transform: translate("+hexa.x+"px,0)rotateY("+hexa.rot+"deg);opacity:" + (90 - hexa.rot)/90+ ";");
                // hexa.g.setAttribute("style", "transform: translate("+hexa.x+"px,0)rotateY("+hexa.rot+"deg)scale("+scale+","+scale+");");
            }
        }
        if (cpt < 19){
            setTimeout(this.hide, 20);
        }
    }
}

export { Hexas };
