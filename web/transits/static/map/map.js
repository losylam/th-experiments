window.onload = function(){
    Celestial.display({
        projection: "aitoff",
        zoomlevel: 3,
        datapath: "static/lib/celestial/data/",
        stars: {proper: false,
                names: false,
                limit: 8,
                data: 'stars.8.json',
                colors: false},
        dsos: {
            display: false,
            names: false
        },
        planets: {
            show: true,
            names: true
        },
        constellations: {
            names: false,
            lines: false,
            lineStyle: { stroke: "#cc00cc", width: 1, opacity: 0.6, dash: [2, 4] }
        },
        mw: {
            show: false
        },
        lines: {  // Display & styles for graticule & some planes
            graticule: { show: false, stroke: "#cccccc", width: 0.6, opacity: 0.8,   
                         // grid values: "outline", "center", or [lat,...] specific position
                         lon: {pos: [""], fill: "#eee", font: "10px Helvetica, Arial, sans-serif"}, 
                         // grid values: "outline", "center", or [lon,...] specific position
                         lat: {pos: [""], fill: "#eee", font: "10px Helvetica, Arial, sans-serif"}},    
            equatorial: { show: false, stroke: "#aaaaaa", width: 1.3, opacity: 0.7 },  
            ecliptic: { show: false, stroke: "#66cc66", width: 1.3, opacity: 0.7 },     
            galactic: { show: false, stroke: "#cc6666", width: 1.3, opacity: 0.7 },    
            supergalactic: { show: false, stroke: "#cc66cc", width: 1.3, opacity: 0.7 }
        },
    });
};
