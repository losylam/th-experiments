import * as THREE from 'https://unpkg.com/three/build/three.module.js';
import { GLTFLoader } from 'https://unpkg.com/three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'https://unpkg.com/three/examples/jsm/controls/OrbitControls.js';

// import { Wireframe } from 'https://unpkg.com/three/examples/jsm/lines/Wireframe.js';
import { GUI } from 'https://unpkg.com/dat.gui@0.7.7/build/dat.gui.module.js';

function create_modelisation(static_path){
    const params = {
        debug: false,
        rotate: 0.0,
    };

    const gui = new GUI();
    const debug_controller = gui.add( params, 'debug' ).name( 'Debug' );
    gui.add( params, 'rotate' ).name( 'Rotation JWST' ).min(-10).max(10).step(0.1);

    function show_helpers(show){
        for (const helper of helpers){
            helper.visible = params.debug;
        }
    }

    debug_controller.onChange(function(){
        show_helpers(params.debug);
        // console.log(params);
    });

    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 1000 );
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild( renderer.domElement );

    const controls = new OrbitControls(camera, renderer.domElement);


    {
        const loader = new THREE.TextureLoader();
        const texture = loader.load(
            static_path + '/modelisation/textures/tychoskymap-md.jpg',
            () => {
                const rt = new THREE.WebGLCubeRenderTarget(texture.image.height);
                rt.fromEquirectangularTexture(renderer, texture);
                scene.background = rt;
            });
    }

    camera.position.z = 20;

    const helpers = [];

    ///////////////////
    // LIGHTS        //
    ///////////////////

    const light = new THREE.AmbientLight( 0xffffff ); // soft white light
    scene.add( light );

    const point_light = new THREE.PointLight( 0xddffff );
    point_light.position.set( 5, 5, 5 );
    scene.add( point_light );

    const point_light_h = new THREE.PointLightHelper( point_light, 1);
    scene.add( point_light_h );
    helpers.push(point_light_h);

    // const material = new THREE.MeshBasicMaterial( { color: 0x999999, wireframe: true, transparent: true });
    // const map = new THREE.Mesh( new THREE.SphereGeometry( 200, 24, 18 ), material);
    // scene.add( map );

    // const polarGridHelper = new THREE.PolarGridHelper( 20, 24, 8, 64, 0x0000ff, 0x808080 );
    // polarGridHelper.position.y = 0;
    // polarGridHelper.position.x = 0;
    // scene.add( polarGridHelper );

    ///////////////////
    // JWST          //
    ///////////////////

    const jwst = new THREE.Group();
    scene.add(jwst);
    jwst.rotation.y = 2.5;

    const loader = new GLTFLoader();
    loader.load(static_path + '/modelisation/jwst/jwst.glb', function(gltf){
        console.log("yep!");
        jwst.add(gltf.scene);
    }, undefined, function (error){
        console.log(error);
    });

    function get_jwst_target(){
        const points = [];
        points.push( new THREE.Vector3( -0.12, 1.48, -100) );
        points.push( new THREE.Vector3( -0.12, 1.48, 0) );
        const geometry = new THREE.BufferGeometry().setFromPoints( points );
        const material = new THREE.LineBasicMaterial( { color: 0x0000ff });
        const line = new THREE.Line( geometry, material );
        return line;
    }

    const jwst_target = get_jwst_target();
    helpers.push(jwst_target);
    jwst.add(jwst_target);

    show_helpers(params.debug);

    ///////////////////
    // ANIMATION     //
    ///////////////////

    let last_anim = new Date();
    function animate(){
        let now = new Date();
        let elapsed = now - last_anim;

        if (jwst){
            jwst.rotation.y += params.rotate * elapsed/10000.0;
        }
        last_anim = now;
        requestAnimationFrame( animate );
        controls.update();
        renderer.render( scene, camera );
    }
    animate();
};

export { create_modelisation };




