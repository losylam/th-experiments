

window.onload = function(){
    const svg = d3.select("svg");
    // const width = +svg.attr("width");
    // const height = +svg.attr("height");

    setTimeout(()=> {
        document.querySelector("#overlay").classList.remove("visible");
    }, 200);
    let socket = io();
    console.log("hello hello");
    socket.on('connect', function(){
        console.log("sio connect");
        socket.emit('message', {data: 'hello io'});
        socket.emit('osc', data=['/transit_ok',  1]);
    });
    let speed = 1;
    let time_scale = 1;

    let amp_noise = 1;
    let amp_sine = 1;

    const vals = [];
    const trs = [];
    const noise = [];
    const sine = [];

    let date_min = 0;
    let date_max = 10000;


    let scale = 1;
    let scale_min = 0;
    let scale_max = 1;
    let position = 0.5;

    for (let i=date_min; i<date_max; i++){
        noise.push(d3.randomNormal(-0.01, 0.01)());
        sine.push(0.1 * Math.sin(i/100));
        let tr = 0.605;
        if (i%1000 < 20){
            tr -= 0.001;
        }
        trs.push(tr);
    }

    function calc(){
        for (let i =0; i< 10000; i++){
            vals[i] = amp_noise * noise[i] + amp_sine * sine[i] + trs[i];
        }
    }

    calc();
    console.log(vals);

    const lerp = (x, y, a) => x * (1 - a) + y * a;
    const clamp = (a, min = 0, max = 1) => Math.min(max, Math.max(min, a));
    const invlerp = (x, y, a) => clamp((a - x) / (y - x));
    const range = (x1, y1, x2, y2, a) => lerp(x2, y2, invlerp(x1, y1, a));

    const canvas_width = 1000;
    const canvas_height = 400;

    let to_scale = function(v){
        return canvas_height * (1 - v);
    };

    const canvas_mx = 100;
    const canvas_my = 40;

    let margin_left = 20;
    let margin_top = 20;
    let val_w = 30;

    function point(x, y, ctx){
        ctx.strokeRect(canvas_mx + x,y,1,1);
    }

    const canvas = document.querySelector("#canvas");
    canvas.style.width = canvas_width + canvas_mx + "px";
    canvas.style.height = canvas_height + 40 + "px";
    canvas.style.position = "absolute";
    canvas.style.top = margin_top + "px";
    canvas.style.left = margin_left + val_w + "px";

    const ctx = canvas.getContext('2d');
    ctx.lineWidth = 2;
    ctx.strokeStyle = "white";
    ctx.fillStyle = "white";

    function draw_scale(ctx){
        ctx.strokeStyle = "gray";
        ctx.lineWidth = 1;
        ctx.beginPath();
        for (let i=1; i<10; i++){
            let y = to_scale(i*0.1);
            ctx.moveTo(canvas_mx, y);
            if (y > 0 && y < canvas_height){
                ctx.lineTo(canvas_mx + canvas_width, y);
            }
        }
        ctx.stroke();
        if (scale < 0.5){
            ctx.beginPath();
            let gr = (0.5 - scale) * 254;
            ctx.strokeStyle = "rgb(" + gr + ", " + gr + ", " + gr + ")";
            for (let s=(1-scale_max)*100; s<(1-scale_min)*100; s++){
                let y = to_scale(Math.floor(s)/100);
                ctx.moveTo(canvas_mx, y);
                if (y > 0 && y < canvas_height){
                    ctx.lineTo(canvas_mx + canvas_width, y);
                }
            }
            ctx.stroke(); 
        }
    }

    function draw(pos){
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        draw_scale(ctx);

        ctx.beginPath();
        ctx.lineWidth = 1.5;
        ctx.setLineDash([3, 3]);
        ctx.strokeStyle = "white";
        ctx.rect(canvas_mx, 0, canvas_width, canvas_height);
        ctx.stroke();

        ctx.setLineDash([]);
        ctx.strokeStyle = "gray";
        ctx.lineWidth = 1.5;
        ctx.beginPath();
        ctx.moveTo(0, scale_min*canvas_height);
        ctx.lineTo(canvas_mx, 0);
        ctx.moveTo(canvas_mx, canvas_height);
        ctx.lineTo(0, scale_max * canvas_height);
        ctx.moveTo(canvas_mx + date_min / 10, canvas_height + canvas_my);
        ctx.lineTo(canvas_mx, canvas_height);
        ctx.moveTo(canvas_mx + canvas_width, canvas_height);
        ctx.lineTo(canvas_mx + date_max / 10, canvas_height + canvas_my);
        ctx.stroke();

        ctx.strokeStyle = "white";
        for (let i=date_min; i<date_max; i++){
            let val = point(range(date_min, date_max, 0, 1000, i), to_scale(vals[(i+Math.floor(pos))%10000]), ctx);
        }


        // draw target
        let pos_x = Math.floor(date_min + (date_max - date_min)/2 + pos)%10000;
        let pos_y = to_scale(vals[pos_x]);
        ctx.strokeStyle = "red";
        ctx.beginPath();
        ctx.arc(canvas_mx + 500, pos_y, 10, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(canvas_mx + 500, 0);
        ctx.lineTo(canvas_mx + 500, pos_y - 10);
        ctx.moveTo(canvas_mx + 500, pos_y + 10);
        ctx.lineTo(canvas_mx + 500, canvas_height);
        ctx.stroke();
        socket.emit('osc', data=['/light',  (canvas_height-pos_y)/canvas_height]);
    }

    let pos = 0;
    setInterval(function(){
        draw(pos);
        pos += speed * time_scale;
        date_slider.style.backgroundPositionX = -pos/10+'px';
    }, 50);

    const val_slider = document.querySelector(".val-slider");
    val_slider.style.position = "absolute";
    val_slider.style.top = margin_top + "px";
    val_slider.style.left = margin_left + "px";
    val_slider.style.width = val_w + "px";
    val_slider.style.height = canvas_height + "px";
    noUiSlider.create(val_slider, {
        start: [0.2, 0.8],
        connect: true,
        range: {
            'min': 0,
            'max': 1
        },
        orientation: "vertical",
        behaviour: 'tap-drag',
    });

    const date_slider = document.querySelector(".date-slider");
    date_slider.style.position = "absolute";
    date_slider.style.top = margin_top + canvas_height + canvas_my + "px";
    date_slider.style.left = margin_left + val_w + canvas_mx + "px";
    date_slider.style.width = canvas_width + "px";
    date_slider.style.height = "50px";
    noUiSlider.create(date_slider, {
        start: [3333, 6666],
        connect: true,
        range: {
            'min': 0,
            'max': 10000
        },
        step: 1,
        behaviour: 'tap-drag',
    });

    function create_slider(selector, min, max, val){
        let new_slider = document.querySelector(selector);
        noUiSlider.create(new_slider, {
            start: [val],
            connect: true,
            range: {
                'min': min,
                'max': max
            },
        });
        return new_slider;
    }

    const speed_slider = create_slider(".speed-slider", 0, 10, 1);
    speed_slider.noUiSlider.on('update', (v, h) => speed = v[0]);

    const amp_noise_slider = create_slider(".amp-noise-slider", 0.00, 1, 1);
    amp_noise_slider.noUiSlider.on('update', function (values, handle){
        amp_noise = values[0];
        calc();
    });

    const amp_sine_slider = create_slider(".amp-sine-slider", 0, 1, 1);
    amp_sine_slider.noUiSlider.on('update', function (values, handle){
        amp_sine = values[0];
        calc();
    });

    val_slider.noUiSlider.on('update', function (values, handle) {
        console.log(values);
        scale_min = values[0];
        position = (scale_min + scale_max)/2;
        scale_max = values[1];
        scale = Math.abs(scale_max-scale_min);
        to_scale = function(v){
            return range(1-values[0], 1-values[1], 0, canvas_height, v);
        };
    });

    date_slider.noUiSlider.on('update', function (values, handle) {
        console.log(values);
        date_min = Math.floor(values[0]);
        date_max = Math.floor(values[1]);
        time_scale = 10000 / (date_max - date_min);
    });

    socket.on("controler", data => {
        if (data.to == "transit"){
            // amp_sine = data.amp_sine;
            amp_sine_slider.noUiSlider.set(data.amp_sine);
            // amp_noise = data.amp_noise;
            amp_noise_slider.noUiSlider.set(data.amp_noise);
            speed_slider.noUiSlider.set(data.speed);
            position = parseFloat(data.position);
            console.log(data);
            let s_min = position - parseFloat(data.zoom_tr)/2;
            let s_max = position + parseFloat(data.zoom_tr)/2;
            val_slider.noUiSlider.set([s_min, s_max]);
            let date_position = parseInt(data.date_position);
            let d_min = date_position - parseInt(data.date_zoom)/2;
            let d_max = date_position + parseInt(data.date_zoom)/2;
            date_slider.noUiSlider.set([d_min, d_max]);
            calc();
        }
    });
};
