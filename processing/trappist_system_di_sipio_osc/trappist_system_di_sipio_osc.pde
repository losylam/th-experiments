//https://disipio.wordpress.com/2012/12/31/simulate-the-solar-system-with-processing/
//datas form https://en.wikipedia.org/wiki/TRAPPIST-1

/*
J'ai tenté de remplacer les données du système solaire par celles de du système trappist...
 Le résultat me positionne face à mon ignorance, à voir ce que l'on peut faire de ça...
 
 // edit laurent
 C'est sûrement la masse de l'étoile qui posait problème (ça me paraissait bizarre qu'elle soit
 100x plus légère que les planêtes)
 
 J'ai aussi:
 - exprimé les orbites par rapport à celle de la planète de référence (C) comme c'est
 celle qui sert de référence pour les masses (mais je ne sais pas si ça change grand chose)
 - ajouté les attractions entre toutes les planètes
 
 Sinon juste changé l'échelle de dessin et la vitesse de la simulation.
 
 // 2020-04-27 - fanch
 Ajout de l'OSC:
 - positions x/y des planètes
 - velocitées x/y des planètes
 - envoi des masses des planètes
 
 */

import traer.physics.*;
import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress puredata;

int speed = 40000; // set the speed

float AU = 1.496e11; // m
float MT = 5.9722e24; //trappistC mass
//float 20 * AU = width/2 // radius of the mini trappist1 system
float G_N = 6.67e-11; // gravitational constant
float m_sun = 1.989e30;
float m_trappistA = 0.080 * m_sun; 
float m_trappistB = 1.017 * MT;
float m_trappistC = 1.156 * MT;
float m_trappistD = 0.297 * MT;
float m_trappistE = 0.772 * MT;
float m_trappistF = 0.934 * MT;
float m_trappistG = 1.148 * MT;
float m_trappistH = 0.331 * MT;
// standard grav parameter mu=GM
float mu_trappist1_system = G_N * m_trappistA; //gravitational constant * star mass
float mu_trappist1_system_reduced = mu_trappist1_system / m_trappistC; // je ne pige pas..

// float r_trappistB = 0.01154775;
// float r_trappistC = 0.01581512; //AU
// float r_trappistD = 0.02228038;
// float r_trappistE = 0.02928285;
// float r_trappistF = 0.03853361;
// float r_trappistG = 0.04687692;
// float r_trappistH = 0.06193488
float	r_trappistB	=	0.730172;
float	r_trappistC	=	1.000000;
float	r_trappistD	=	1.408802;
float	r_trappistE	=	1.851573;
float	r_trappistF	=	2.436504;
float	r_trappistG	=	2.964057;
float	r_trappistH	=	3.916181;
ParticleSystem physics;
Particle trappistA; //star
Particle trappistB; //trappistB
Particle trappistC; //trappistC
Particle trappistD; //trappistD
Particle trappistE; //trappistE
Particle trappistF; //trappistF
Particle trappistG; //trappistG
Particle trappistH; //trappistH


void setup()
{ 
  size(900, 900);
  smooth();
  
  oscP5 = new OscP5(this, 12000);
  // PureData address
  puredata = new NetAddress("127.0.0.1", 8100); 
  
  float r_0 = ( width/10 );
  fill( 0 );
  ellipseMode( CENTER );

  println( "mu of the trappist1 system " + mu_trappist1_system );
  println( "speed of trappistC at perihelion = " + sqrt(mu_trappist1_system/AU) + " m/s");
  println(" width = " + width + " r_0 = " + r_0 );

  physics = new ParticleSystem( 0., 0. );
  physics.setIntegrator( ParticleSystem.RUNGE_KUTTA );

  trappistA = physics.makeParticle( m_trappistA/m_trappistC, width/2, height/2, 0 );
  trappistA.makeFixed();

  trappistB = physics.makeParticle( m_trappistB/m_trappistC, width/2 + r_0*r_trappistB, height/2, 0 );
  trappistB.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistB/r_0), 0. );

  trappistC = physics.makeParticle( 1, width/2 + r_0*r_trappistC, height/2, 0 );
  trappistC.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistC/r_0), 0. );

  trappistD = physics.makeParticle( m_trappistD/m_trappistC, width/2 + r_0*r_trappistD, height/2, 0 );
  trappistD.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistD/r_0), 0. );

  trappistE = physics.makeParticle( m_trappistE/m_trappistC, width/2 + r_0*r_trappistE, height/2, 0 );
  trappistE.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistE/r_0), 0. );

  trappistF = physics.makeParticle( m_trappistF/m_trappistC, width/2 + r_0*r_trappistF, height/2, 0 );
  trappistF.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistF/r_0), 0. );

  trappistG = physics.makeParticle( m_trappistG/m_trappistC, width/2 + r_0*r_trappistG, height/2, 0 );
  trappistG.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistG/r_0), 0. );

  trappistH = physics.makeParticle( m_trappistH/m_trappistC, width/2 + r_0*r_trappistH, height/2, 0 );
  trappistH.velocity().set( 0, sqrt(mu_trappist1_system_reduced/r_trappistH/r_0), 0. ); 

  println( "Start:" );
  println( "vx = " + trappistC.velocity().x() + " vy = " + trappistC.velocity().y() );

  physics.makeAttraction( trappistA, trappistB, G_N, r_0/100 );
  physics.makeAttraction( trappistA, trappistC, G_N, r_0/100 );
  physics.makeAttraction( trappistA, trappistD, G_N, r_0/100 );
  physics.makeAttraction( trappistA, trappistE, G_N, r_0/100 );
  physics.makeAttraction( trappistA, trappistF, G_N, r_0/100 );
  physics.makeAttraction( trappistA, trappistG, G_N, r_0/100 );
  physics.makeAttraction( trappistA, trappistH, G_N, r_0/100 );

  physics.makeAttraction( trappistB, trappistC, G_N, r_0/100 );
  physics.makeAttraction( trappistB, trappistD, G_N, r_0/100 );
  physics.makeAttraction( trappistB, trappistE, G_N, r_0/100 );
  physics.makeAttraction( trappistB, trappistF, G_N, r_0/100 );
  physics.makeAttraction( trappistB, trappistG, G_N, r_0/100 );
  physics.makeAttraction( trappistB, trappistH, G_N, r_0/100 );

  physics.makeAttraction( trappistC, trappistD, G_N, r_0/100 );
  physics.makeAttraction( trappistC, trappistE, G_N, r_0/100 );
  physics.makeAttraction( trappistC, trappistF, G_N, r_0/100 );
  physics.makeAttraction( trappistC, trappistG, G_N, r_0/100 );
  physics.makeAttraction( trappistC, trappistH, G_N, r_0/100 );

  physics.makeAttraction( trappistD, trappistE, G_N, r_0/100 );
  physics.makeAttraction( trappistD, trappistF, G_N, r_0/100 );
  physics.makeAttraction( trappistD, trappistG, G_N, r_0/100 );
  physics.makeAttraction( trappistD, trappistH, G_N, r_0/100 );

  physics.makeAttraction( trappistE, trappistF, G_N, r_0/100 );
  physics.makeAttraction( trappistE, trappistG, G_N, r_0/100 );
  physics.makeAttraction( trappistE, trappistH, G_N, r_0/100 );

  physics.makeAttraction( trappistF, trappistG, G_N, r_0/100 );
  physics.makeAttraction( trappistF, trappistH, G_N, r_0/100 );

  physics.makeAttraction( trappistG, trappistH, G_N, r_0/100 );

}
void mousePressed()
{
}
void mouseDragged()
{
}
void mouseReleased()
{
}
void draw()
{
  //println( "(x, y) = ( " + trappistC.position().x() + ", " + trappistC.position().y() + ")" + " (vx, vy) = ( " + trappistC.velocity().x() + ", " + trappistC.velocity().y() + ")");
  //oscP5.send(new OscMessage("/trappistB-pos-x").add((trappistB.position().x()) + (" ") + (trappistB.position().y()), puredata));

  OscMessage BMessage = new OscMessage("/trappistB");
  BMessage.add(int(trappistB.position().x()) + (" "));
  BMessage.add(int(trappistB.position().y()) + (" "));
  BMessage.add((trappistB.velocity().x() * (speed)) + (" "));
  BMessage.add((trappistB.velocity().y() * (speed)) + (" "));
  BMessage.add((m_trappistB / MT));
  oscP5.send(BMessage, puredata);

  OscMessage CMessage = new OscMessage("/trappistC");
  CMessage.add(int(trappistC.position().x()) + (" "));
  CMessage.add(int(trappistC.position().y()) + (" "));
  CMessage.add((trappistC.velocity().x() * (speed)) + (" "));
  CMessage.add((trappistC.velocity().y() * (speed)) + (" "));
  CMessage.add((m_trappistC / MT));
  oscP5.send(CMessage, puredata);
  

  OscMessage DMessage = new OscMessage("/trappistD");
  DMessage.add(int(trappistD.position().x()) + (" "));
  DMessage.add(int(trappistD.position().y()) + (" "));
  DMessage.add((trappistD.velocity().x() * (speed)) + (" "));
  DMessage.add((trappistD.velocity().y() * (speed)) + (" ")); 
  oscP5.send(DMessage, puredata);

  OscMessage EMessage = new OscMessage("/trappistE");
  EMessage.add(int(trappistE.position().x()) + (" "));
  EMessage.add(int(trappistE.position().y()) + (" "));
  EMessage.add((trappistE.velocity().x() * (speed)) + (" "));
  EMessage.add((trappistE.velocity().y() * (speed)) + (" ")); 
  EMessage.add((m_trappistE / MT));
  oscP5.send(EMessage, puredata);

  OscMessage FMessage = new OscMessage("/trappistF");
  FMessage.add(int(trappistF.position().x()) + (" "));
  FMessage.add(int(trappistF.position().y()) + (" "));
  FMessage.add((trappistF.velocity().x() * (speed)) + (" "));
  FMessage.add((trappistF.velocity().y() * (speed)) + (" ")); 
  FMessage.add((m_trappistF / MT));
  oscP5.send(FMessage, puredata);

  OscMessage GMessage = new OscMessage("/trappistG");
  GMessage.add(int(trappistG.position().x()) + (" "));
  GMessage.add(int(trappistG.position().y()) + (" "));
  GMessage.add((trappistG.velocity().x() * (speed)) + (" "));
  GMessage.add((trappistG.velocity().y() * (speed)) + (" ")); 
  GMessage.add((m_trappistG / MT));
  oscP5.send(GMessage, puredata);

  OscMessage HMessage = new OscMessage("/trappistH");
  HMessage.add(int(trappistH.position().x()) + (" "));
  HMessage.add(int(trappistH.position().y()) + (" "));
  HMessage.add((trappistH.velocity().x() * (speed)) + (" "));
  HMessage.add((trappistH.velocity().y() * (speed)) + (" "));
  HMessage.add((m_trappistH / MT));
  oscP5.send(HMessage, puredata);

  delay (10); 
  physics.tick(speed);
  background( 0 );

  color y = color( 255, 204, 0);
  fill(y);
  noStroke();
  ellipse( trappistA.position().x(), trappistA.position().y(), 12, 12 );

  color g = color( 0, 200, 100);
  fill(g);
  ellipse( trappistB.position().x(), trappistB.position().y(), 5, 5 );

  color b = color( 0, 100, 230);
  fill(b);
  ellipse( trappistC.position().x(), trappistC.position().y(), 5, 5 );

  color r = color( 255, 0, 0);
  fill(r);
  ellipse( trappistD.position().x(), trappistD.position().y(), 3, 3 );

  color j = color( 255, 150, 100);
  fill(j);
  ellipse( trappistE.position().x(), trappistE.position().y(), 10, 10 ); 

  color s = color( 100, 110, 100);
  fill(s);
  ellipse( trappistF.position().x(), trappistF.position().y(), 15, 4 ); 
  color w = color( 200, 150, 100 );
  fill(w); 
  ellipse( trappistF.position().x(), trappistF.position().y(), 8, 8 );

  color b2 = color( 0, 200, 200);
  fill(b2);
  ellipse( trappistG.position().x(), trappistG.position().y(), 8, 8 );

  color b3 = color( 0, 0, 250);
  fill(b3);
  ellipse( trappistH.position().x(), trappistH.position().y(), 8, 8 );
}
