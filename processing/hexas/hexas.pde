float ANGLE = TWO_PI / 6.0;
float cpt;
float s = 200;
float ratio = sqrt(3)/3;
boolean b_export = false;

float ease(float p, float g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}
float ease(float p) {
  return 3*p*p - 2*p*p*p;
}

void setup() {
  size(800, 800);
  stroke(255);
  strokeWeight(1.5);
}

void draw() {
  background(255);
  s += 8;
  //s = map(mouseX, 0, width, 200, 600);
  float pos = (s-200)/400;
  float loc_s = 200 + ease(pos, 1)*400;
  println(pos);
  float tr = map(loc_s, 200, 600, 600, 1200);
  //if (pos<0.5){
  //float tr = 600 + map(cos(PI*pos), 1, -1, 0, 1) * 600;
  //}
  //println(tr);

  translate(width/2, height/2+ tr);
  rotate(-ANGLE);
  cpt = 0;
  hexas(0, 0, loc_s*3);
  //s = 1.08;
  //hexas(0, 0, 200*(sqrt(3)/3));
  //circle(0, 0, 200);
  if (s > 600) {
    noLoop();
  } else {
    if (b_export){
      saveFrame();
    }
  }
}

void hexa(float x, float y, float radius) {
  radius = radius*ratio;
  beginShape();
  for (float a = 0; a < TWO_PI; a += ANGLE) {
    float sx = x + cos(a) * radius;
    float sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

void hexas(float x, float y, float radius) {
  cpt += 1;
  float start = ANGLE/2;
  if (cpt%2 == 0) {
    start = start + PI;
  }
  for (int i = 0; i< 6; i++) {

    float sx = x + cos(start + i*ANGLE) * radius;
    float sy = y + sin(start + i*ANGLE) * radius;
    

    fill(0);
    hexa(sx, sy, radius);
    
    if (cpt == 1 && i==5) {
      float sxx = x + cos(start + i*ANGLE) * 3*radius;
      float syy = y + sin(start + i*ANGLE) * 3*radius;
     // fill(100);
      hexa(sxx, syy,3*radius);
    }


    if (i == 5 && cpt < 9) {
      hexas(sx, sy, radius/3);
    }
  }
  
    fill(255);
    hexa(x, y, radius);
}
